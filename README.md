**Checkout and build**

git clone https://gitlab.com/geoAtCourantyne/ubsFXIT181221.git
cd ubsFXIT181221.git

mvn clean test
``

**Solution Notes**

The exercise specifies  all updates called in a single thread - therefore no locking or other MT techniques in the given solution.

All markets are treated the same see javadoc comments in VwapCalculator for possible enhancements

Indicative prices for an instrument from any market set the state of the vwap price for that instrument to indicative. 

When the first firm price for that instrument arrives the vwap price is reset to that update wit a state of firm.
 I acknowledge that this is pretty simplistic but the javadoc comments in VwapCalculator provide possible enhancements
 
This first cut solution uses a deque to hold the updates up to a bounded size of 10 most recent updates to reduce amount of memory consumed.
 
The InstrumentCalc recalculates the vwap by applying a delta to the current values, removing the oldest updates contribution before applying the new updates values.
This avoids the cost of repeatedly iterating the collection of updates.

Note: no account is taken of precision in prices and amounts - its assumed they are in standard 'tick' sizes 



**Actual Problem**
For this task, the candidate is expected to demonstrate good software design and implementation within the context of low-latency (high throughput) systems.  The solution provided is expected to be of reasonable quality and take consideration of performance.  Wherever the problem is unclear, the candidate should make (and state) any assumptions he or she thinks appropriate within the application context. Finally, although there is no time limit, this task is expected to take no longer than an hour.
  
The task: 

Design and write an implementation of the calcspec.Calculator interface below, in Java, so on the applyMarketUpdate method being called the calcspec.TwoWayPrice returned is the VWAP two-way price for the instrument of the calcspec.MarketUpdate. Each instrument can receive a two way price update from one of 50 markets, and in calculating the VWAP for the instrument the calculator should consider the most recent price update for each market (received so far)

The VWAP two-way price for an instrument is defined as:

Bid = Sum(calcspec.Market Bid Price * calcspec.Market Bid Amount)/ Sum(calcspec.Market Bid Amount)
Offer = Sum(calcspec.Market Offer Price * calcspec.Market Offer Amount)/ Sum(calcspec.Market Offer Amount)


It should be assumed the process calling calcspec.Calculator.applyMarketUpdate is single threaded.


public interface calcspec.Calculator {
    calcspec.TwoWayPrice applyMarketUpdate(final calcspec.MarketUpdate twoWayMarketPrice);
}

public interface calcspec.MarketUpdate {
    calcspec.Market getMarket();
    calcspec.TwoWayPrice getTwoWayPrice();
}

public interface calcspec.TwoWayPrice {
    calcspec.Instrument getInstrument();
    calcspec.State getState();
    double getBidPrice();
    double getOfferAmount();
    double getOfferPrice();
    double getBidAmount();
}

public enum calcspec.Instrument {
    INSTRUMENT0,
…
    INSTRUMENT19
}

public enum calcspec.Market {
    MARKET0,
…
    MARKET49
}

public enum calcspec.Side {
    BID,
    OFFER
}

public enum calcspec.State {
    FIRM,
    INDICATIVE
}
