package calcspec;

/**
 * Created by georgemullen on 22/04/2017.
 */
public interface MarketUpdate {
    Market getMarket();
    TwoWayPrice getTwoWayPrice();
}
