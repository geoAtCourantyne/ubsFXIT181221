package calcspec; /**
 * Created by georgemullen on 22/04/2017.
 */

public interface Calculator {
    TwoWayPrice applyMarketUpdate(final MarketUpdate twoWayMarketPrice);
}