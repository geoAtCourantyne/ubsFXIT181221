package calcspec;

/**
 * Created by georgemullen on 22/04/2017.
 */
public interface TwoWayPrice {
    Instrument getInstrument();
    State getState();
    double getBidPrice();
    double getOfferAmount();
    double getOfferPrice();
    double getBidAmount();
}