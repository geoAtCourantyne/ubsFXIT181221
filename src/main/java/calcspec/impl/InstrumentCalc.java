package calcspec.impl;

import calcspec.*;

import java.util.ArrayDeque;
import java.util.Deque;

/**
 * Created by georgemullen on 22/04/2017.
 *
 * As an initial cut to easily access the most recent and oldest
 *      update I store them in a {@link Deque}
 *  The actual vwap adjustment is done in {@link VwapTwoWayPrice}
 */
public class InstrumentCalc implements Calculator{

    /**
     *  Constrains the max memory holding previous updates.
     */
    public static int NUMBER_OF_UPDATES_TO_AVERAGE_OVER = 10; // onlv take vwap over maximum of this number of recent updates sould make this configurable



    Deque<TwoWayPrice> updateQ = new ArrayDeque<>(NUMBER_OF_UPDATES_TO_AVERAGE_OVER) ;
    VwapTwoWayPrice vwapTwoWayPrice;

    public InstrumentCalc(MarketUpdate mktUpdate)
    {
        Instrument i = mktUpdate.getTwoWayPrice().getInstrument();
        State s = mktUpdate.getTwoWayPrice().getState();
        vwapTwoWayPrice = new VwapTwoWayPrice(i, s);
    }

    public TwoWayPrice applyMarketUpdate(MarketUpdate twoWayMarketPrice) {
        TwoWayPrice oldUpdate = null;
        TwoWayPrice newUpdate = twoWayMarketPrice.getTwoWayPrice();
        if(updateQ.size()== NUMBER_OF_UPDATES_TO_AVERAGE_OVER)  // only keep the most recent set of updates
        {
            oldUpdate = updateQ.removeLast();

        }
        updateQ.addFirst(newUpdate);
        this.vwapTwoWayPrice.update(newUpdate, oldUpdate);
        return this.vwapTwoWayPrice;
    }
}
