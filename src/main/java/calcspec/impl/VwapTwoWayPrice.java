package calcspec.impl;
import calcspec.Instrument;
import calcspec.State;
import calcspec.TwoWayPrice;
/**
 * Created by georgemullen on 22/04/2017.
 */
class VwapTwoWayPrice implements TwoWayPrice {
    private Instrument instrument;
    private State state;
    private double vwBid;
    private double vwOfferQty;
    private double vwOffer;
    private double vwBidQty;

    public VwapTwoWayPrice(Instrument instrument, State s) {
        this.instrument = instrument;
        this.state = s;
    }

    @Override
    public Instrument getInstrument() {
        return this.instrument;
    }

    @Override
    public State getState() {
        return this.state;
    }

    @Override
    public double getBidPrice() {
        return this.vwBid;
    }

    @Override
    public double getOfferAmount() {
        return this.vwOfferQty;
    }

    @Override
    public double getOfferPrice() {
        return this.vwOffer;
    }

    @Override
    public double getBidAmount() {
        return this.vwBidQty;
    }

    /**
     *
     * @param newUpdate if not null  - add this contribution to the bid and offer vwap calculation
     *   Note if the status of newUpdate is INDICATIVE then vwap will continue to be calculated but the
     *                 status of this {@link VwapTwoWayPrice}  will be set to INDICATIVE.
     *   When the next newUpdate with status FIRM arrives, the values of bid, offer , bidAmount, offerAmount
     *                  will be set to those of the newUpdate and the state of this {@link VwapTwoWayPrice}
     *   will revert to FIRM.
     *
     * @param oldUpdate if not null - remove this contribution from the bid and offer vwap calculation
     */
    public void update(TwoWayPrice newUpdate, TwoWayPrice oldUpdate) {
       if (isIndicative(this.state) && !isIndicative(newUpdate))
        {
            // first non-indicative update after previous indicative updates- reset  vwaps and use firm value only
            this.vwOffer = newUpdate.getOfferPrice();
            this.vwOfferQty = newUpdate.getOfferAmount();
            this.vwBid = newUpdate.getBidPrice();
            this.vwBidQty= newUpdate.getBidAmount();
        }
        else
       {
           applyDeltaUpdate(true, newUpdate, oldUpdate);
           applyDeltaUpdate(false, newUpdate, oldUpdate);
       }
        this.state = newUpdate.getState();
    }

    static boolean isIndicative(TwoWayPrice p )
    {
        return isIndicative(p.getState());
    }

    static boolean isIndicative(State s )
    {
        return State.INDICATIVE.equals(s);
    }

    private double reCalcQty(double currentQty, double oldQty, double newQty )
    {
        return currentQty - oldQty + newQty;
    }

    private double reCalcWeightedSum(double currentPrice, double currentQty, double oldPrice, double oldQty, double newPrice, double newQty)
    {
        return  currentQty*currentPrice - (oldQty*oldPrice) + (newQty * newPrice);
    }

    void applyDeltaUpdate(boolean isBid, TwoWayPrice newUpdate, TwoWayPrice oldUpdate)
    {
        double currentVwapPrice, currentVwapQty, oldPrice=0.0, oldQty=0.0, newPrice = 0.0, newQty = 0.0;
        currentVwapPrice = (isBid) ? this.vwBid : this.vwOffer;
        currentVwapQty = (isBid) ? this.vwBidQty : this.vwOfferQty;
        if (newUpdate != null) {

            newPrice = (isBid) ? newUpdate.getBidPrice() : newUpdate.getOfferPrice();
            newQty = (isBid) ? newUpdate.getBidAmount() : newUpdate.getOfferAmount();

        }
        if (oldUpdate != null)
        {
            oldPrice = (isBid) ? oldUpdate.getBidPrice() : oldUpdate.getOfferPrice();
            oldQty = (isBid) ? oldUpdate.getBidAmount() : oldUpdate.getOfferAmount();
        }
        double qty = reCalcQty(currentVwapQty, oldQty, newQty);
        double ws = reCalcWeightedSum(currentVwapPrice, currentVwapQty,
                oldPrice, oldQty,
                newPrice, newQty);

        if(isBid) {
            this.vwBid = ws / qty;
            this.vwBidQty = qty;
        }
        else
        {
            this.vwOffer= ws / qty;
            this.vwOfferQty = qty;
        }
    }
}
