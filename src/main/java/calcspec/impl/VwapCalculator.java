package calcspec.impl;

import calcspec.*;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by georgemullen on 22/04/2017.
 *
 * Key assumption that  {@link Calculator::applyMarketUpdate} is  always called in a single thread.
 *      Therefore there is no locking or other MT strategy present in the implementation
 * All Vwap calculations for a particular instrument a delegated to {@link InstrumentCalc} Calculator
 * All Markets are treated equivalently
 * A simple extension would be to have separate InstrumentCalcs for each Market
 *      and report back the weighted sum of each markets instrument vwap.
 *      This would allow indicative prices from certain markets to be removed from the total vwap
 *      reported for an instrument.
 *
 * However a here the strategy is to continue calculating vwap with indicative prices from any market
 * but to set the state in the instrument's {@link TwoWayPrice} vwap to State.Indicative.
 *      When the first subsequent update with State.FIRM is applied then the instrument's {@link TwoWayPrice vwap }
 *      is reset to the values in the {@link MarketUpdate} and the state is reset to FIRM.
 *
 * Vwap is only calculated on up to a maximum of the 10 most recent updates, its done by arithemetically removing the oldest
 *      update and add the newest update values to the current vwap value.
 *      See {@link InstrumentCalc}  and {@link VwapTwoWayPrice} for more details
 *
 */
public class VwapCalculator implements Calculator {

    Map<Instrument, InstrumentCalc> map = new HashMap<>();

    // always called from a single thread
    // all markets applied
    // indicative prices ignored
    public TwoWayPrice applyMarketUpdate(final MarketUpdate twoWayMarketPrice) {
        Instrument instrument = twoWayMarketPrice.getTwoWayPrice().getInstrument();
        InstrumentCalc instrumentVwapCalc = map.computeIfAbsent(instrument, x -> new InstrumentCalc(twoWayMarketPrice));
        return instrumentVwapCalc.applyMarketUpdate(twoWayMarketPrice);
    }
}
