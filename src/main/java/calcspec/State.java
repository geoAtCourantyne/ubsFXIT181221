package calcspec;

/**
 * Created by georgemullen on 22/04/2017.
 */

public enum State {
    FIRM,
    INDICATIVE
}
