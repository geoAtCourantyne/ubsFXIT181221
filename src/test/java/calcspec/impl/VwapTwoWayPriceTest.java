package calcspec.impl;
import calcspec.Instrument;
import calcspec.State;
import calcspec.TwoWayPrice;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by georgemullen on 22/04/2017.
 */
public class VwapTwoWayPriceTest {


    private VwapTwoWayPrice testClazz;

    @Before
    public void setup() {
        this.testClazz = new VwapTwoWayPrice(Instrument.INSTRUMENT0, State.FIRM);
    }

    @Test
    public void update() throws Exception {
        double testbid = 1.5;
        double testbidqty = 100.0;
        TwoWayPrice mockNewUpdate = mock(TwoWayPrice.class, "newupdate");
        when(mockNewUpdate.getBidAmount()).thenReturn(testbidqty);
        when(mockNewUpdate.getBidPrice()).thenReturn(testbid);
        when(mockNewUpdate.getState()).thenReturn(State.FIRM);
        this.testClazz.update(mockNewUpdate, null);
        assertEquals(State.FIRM, testClazz.getState());
        assertEquals(testbidqty, this.testClazz.getBidAmount(), 1e-9);
        assertEquals(testbid, this.testClazz.getBidPrice(), 1e-9);
    }


    TwoWayPrice createMockUpdate(double bidqty, double bid, String name) {
        return createMockUpdate(bidqty, bid, name, State.FIRM);
    }


    TwoWayPrice createMockUpdate(double bidqty, double bid, String name, State state) {
        TwoWayPrice mockNewUpdate = mock(TwoWayPrice.class, name);
        when(mockNewUpdate.getBidAmount()).thenReturn(bidqty);
        when(mockNewUpdate.getBidPrice()).thenReturn(bid);
        when(mockNewUpdate.getState()).thenReturn(state);
        return mockNewUpdate;
    }
    @Test
    public void updates_less_than_capacity() throws Exception {


        double[] testbid = new double[]{1.5, 1.34, 1.67, 2.44};
        double[] testbidqty = new double[]{100.0, 250.0, 300.0, 210.0};
        TwoWayPrice[] mockupdates = new TwoWayPrice[testbid.length];


        double sum = 0.0;
        double wsum = 0.0;
        for (int i = 0; i < testbid.length; i++) {
            sum += testbidqty[i];
            wsum += (testbidqty[i] * testbid[i]);
            mockupdates[i] = createMockUpdate(testbidqty[i], testbid[i], "newupdate" + i);
        }
        double expectedvwap = wsum / sum;
        TwoWayPrice mockOldUpdate = mock(TwoWayPrice.class, "oldupdate");
        this.testClazz.update(mockupdates[0], null);
        this.testClazz.update(mockupdates[1], null);
        this.testClazz.update(mockupdates[2], null);
        this.testClazz.update(mockupdates[3], null);
        assertEquals(State.FIRM, testClazz.getState());
        assertEquals(sum, this.testClazz.getBidAmount(), 1e-9);
        assertEquals(expectedvwap, this.testClazz.getBidPrice(), 1e-9);
    }

    @Test
    public void updates_at_full_capacity() throws Exception {
        double[] testbid = new double[]{1.5, 1.34, 1.67, 2.44};
        double[] testbidqty = new double[]{100.0, 250.0, 300.0, 100.0};
        TwoWayPrice[] mockupdates = new TwoWayPrice[testbid.length];
        double sum = 0.0;
        double wsum = 0.0;
        for (int i = 0; i < testbid.length; i++) {
            sum += testbidqty[i];
            wsum += (testbidqty[i] * testbid[i]);
            mockupdates[i] = createMockUpdate(testbidqty[i], testbid[i], "newupdate" + i);
        }
        double expectedvwap = wsum / sum;
        TwoWayPrice mockOldUpdate = mock(TwoWayPrice.class, "oldupdate");
        this.testClazz.update(mockupdates[0], null);
        this.testClazz.update(mockupdates[1], null);
        this.testClazz.update(mockupdates[2], null);
        this.testClazz.update(mockupdates[3], null);
        assertEquals(State.FIRM, testClazz.getState());
        assertEquals(sum, this.testClazz.getBidAmount(), 1e-9);
        assertEquals(expectedvwap, this.testClazz.getBidPrice(), 1e-9);

        this.testClazz.update(mockupdates[3], mockupdates[0]);
        this.testClazz.update(mockupdates[3], mockupdates[1]);
        this.testClazz.update(mockupdates[3], mockupdates[2]);
        this.testClazz.update(mockupdates[3], mockupdates[3]);

        sum = testbidqty[3]*4;
        expectedvwap = testbid[3];
        assertEquals(sum, this.testClazz.getBidAmount(), 1e-9);
        assertEquals(expectedvwap, this.testClazz.getBidPrice(), 1e-9);
    }


    @Test
    public void updates_at_indicative_when_change_to_firm() throws Exception {
        double[] testbid = new double[]{1.5, 1.34, 1.67, 2.44};
        double[] testbidqty = new double[]{100.0, 250.0, 300.0, 210.0};
        State[] testStates = new State[]{State.INDICATIVE, State.INDICATIVE, State.INDICATIVE, State.FIRM};
        TwoWayPrice[] mockupdates = new TwoWayPrice[testbid.length];
        double sum = 0.0;
        double wsum = 0.0;
        for (int i = 0; i < testbid.length; i++) {
            sum += testbidqty[i];
            wsum += (testbidqty[i] * testbid[i]);
            mockupdates[i] = createMockUpdate(testbidqty[i], testbid[i], "newupdate" + i, testStates[i]);
        }

        this.testClazz.update(mockupdates[0], null);
        this.testClazz.update(mockupdates[1], null);
        this.testClazz.update(mockupdates[2], null);
        this.testClazz.update(mockupdates[3], null);
        assertEquals(State.FIRM, testClazz.getState());
        assertEquals(mockupdates[3].getBidAmount(), this.testClazz.getBidAmount(), 1e-9);
        assertEquals(mockupdates[3].getBidPrice(), this.testClazz.getBidPrice(), 1e-9);
    }


}