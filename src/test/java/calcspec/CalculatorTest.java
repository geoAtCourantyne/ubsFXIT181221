package calcspec;

import calcspec.impl.VwapCalculator;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * Created by georgemullen on 22/04/2017.
 */
public class CalculatorTest {


    private Calculator testClazz;

    @Before
    public void setup() {
        this.testClazz = new VwapCalculator();
    }


    private MarketUpdate createMarketUpdate(Market mkt, Instrument instrument, State state, double bid, double bidQty, double offer, double offerQty)
    {
        TwoWayPrice mockMktUpdate = createMockTwoWayPrice(instrument, state, bid, bidQty, offer, offerQty);
        MarketUpdate mockUpdate = mock(MarketUpdate.class);
        when(mockUpdate.getMarket()).thenReturn(mkt);
        when(mockUpdate.getTwoWayPrice()).thenReturn(mockMktUpdate);
        return mockUpdate;
    }

    TwoWayPrice createMockTwoWayPrice(Instrument instrument, State state, double bid, double bidQty, double offer, double offerQty)
    {
        TwoWayPrice mockMktUpdate = mock(TwoWayPrice.class);
        when(mockMktUpdate.getInstrument()).thenReturn(instrument);
        when(mockMktUpdate.getState()).thenReturn(state);
        when(mockMktUpdate.getBidAmount()).thenReturn(bidQty);
        when(mockMktUpdate.getBidPrice()).thenReturn(bid);
        when(mockMktUpdate.getOfferAmount()).thenReturn(offerQty);
        when(mockMktUpdate.getOfferPrice()).thenReturn(offer);
        return mockMktUpdate;
    }


    @Test
    public void applyMarketUpdate_single_update_values() {
        TwoWayPrice mockMktUpdate = createMockTwoWayPrice(Instrument.INSTRUMENT0, State.FIRM, 90.0, 100.0, 110.0, 50.0);
        MarketUpdate mockUpdate = mock(MarketUpdate.class);
        when(mockUpdate.getMarket()).thenReturn(Market.MARKET0);
        when(mockUpdate.getTwoWayPrice()).thenReturn(mockMktUpdate);

        TwoWayPrice actual = this.testClazz.applyMarketUpdate(mockUpdate);

        assertNotNull(actual);
        assertEquals(Instrument.INSTRUMENT0, actual.getInstrument());
        assertEquals(State.FIRM, actual.getState());
        assertEquals(100.0, actual.getBidAmount(), 1e-9);
        assertEquals(90.0, actual.getBidPrice(), 1e-9);
        assertEquals(50.0, actual.getOfferAmount(), 1e-9);
        assertEquals(110.00, actual.getOfferPrice(), 1e-9);
    }




    @Test
    public void applyMarketUpdate_status() {
        MarketUpdate mockUpdateFirm = createMarketUpdate(Market.MARKET0, Instrument.INSTRUMENT0, State.FIRM, 90.0, 100.0, 110.0, 50.0);
        MarketUpdate mockUpdateIndicative  = createMarketUpdate(Market.MARKET0, Instrument.INSTRUMENT0, State.INDICATIVE, 70.0, 10.0, 150.0, 5.0);
        MarketUpdate mockUpdateFirm2 = createMarketUpdate(Market.MARKET0, Instrument.INSTRUMENT0, State.FIRM, 95.0, 200.0, 105.0, 25.0);
        TwoWayPrice actual = this.testClazz.applyMarketUpdate(mockUpdateFirm);
        assertNotNull(actual);
        assertEquals(Instrument.INSTRUMENT0, actual.getInstrument());
        assertEquals(State.FIRM, actual.getState());
        actual = this.testClazz.applyMarketUpdate(mockUpdateIndicative);
        assertEquals(State.INDICATIVE, actual.getState());
        actual = this.testClazz.applyMarketUpdate(mockUpdateFirm2);
        assertEquals(State.FIRM, actual.getState());
    }

    @Test
    public void applyMarketUpdate_differentMarketsDifferentInstruments() {
        MarketUpdate mockUpdateM0 = createMarketUpdate(Market.MARKET0, Instrument.INSTRUMENT0, State.FIRM, 90.0, 100.0, 110.0, 50.0);
        MarketUpdate mockUpdateM1 = createMarketUpdate(Market.MARKET1, Instrument.INSTRUMENT1, State.FIRM, 91.0, 101.0, 111.0, 51.0);
        MarketUpdate mockUpdateM2 = createMarketUpdate(Market.MARKET2, Instrument.INSTRUMENT2, State.FIRM, 92.0, 102.0, 112.0, 52.0);

        TwoWayPrice actual0 = this.testClazz.applyMarketUpdate(mockUpdateM0);
        assertEquals(Instrument.INSTRUMENT0, actual0.getInstrument());
        assertEquals(State.FIRM, actual0.getState());
        assertEquals(100.0, actual0.getBidAmount(), 1e-9);
        assertEquals(90.0, actual0.getBidPrice(), 1e-9);
        assertEquals(50.0, actual0.getOfferAmount(), 1e-9);
        assertEquals(110.00, actual0.getOfferPrice(), 1e-9);


        TwoWayPrice actual1 = this.testClazz.applyMarketUpdate(mockUpdateM1);
        assertEquals(Instrument.INSTRUMENT1, actual1.getInstrument());
        assertEquals(State.FIRM, actual1.getState());
        assertEquals(101.0, actual1.getBidAmount(), 1e-9);
        assertEquals(91.0, actual1.getBidPrice(), 1e-9);
        assertEquals(51.0, actual1.getOfferAmount(), 1e-9);
        assertEquals(111.00, actual1.getOfferPrice(), 1e-9);


        TwoWayPrice actual2 = this.testClazz.applyMarketUpdate(mockUpdateM2);
        assertEquals(Instrument.INSTRUMENT2, actual2.getInstrument());
        assertEquals(State.FIRM, actual2.getState());
        assertEquals(102.0, actual2.getBidAmount(), 1e-9);
        assertEquals(92.0, actual2.getBidPrice(), 1e-9);
        assertEquals(52.0, actual2.getOfferAmount(), 1e-9);
        assertEquals(112.00, actual2.getOfferPrice(), 1e-9);

    }

    @Test
    public void applyMarketUpdate_differentMarketSameInstruments() {
        MarketUpdate mockUpdateM0 = createMarketUpdate(Market.MARKET0, Instrument.INSTRUMENT5, State.FIRM, 90.0, 100.0, 110.0, 50.0);
        MarketUpdate mockUpdateM1 = createMarketUpdate(Market.MARKET1, Instrument.INSTRUMENT5, State.FIRM, 91.0, 101.0, 111.0, 51.0);
        MarketUpdate mockUpdateM2 = createMarketUpdate(Market.MARKET2, Instrument.INSTRUMENT5, State.FIRM, 92.0, 102.0, 112.0, 52.0);

        TwoWayPrice actual0 = this.testClazz.applyMarketUpdate(mockUpdateM0);
        assertEquals(Instrument.INSTRUMENT5, actual0.getInstrument());
        assertEquals(State.FIRM, actual0.getState());
        assertEquals(100.0, actual0.getBidAmount(), 1e-9);
        assertEquals(90.0, actual0.getBidPrice(), 1e-9);
        assertEquals(50.0, actual0.getOfferAmount(), 1e-9);
        assertEquals(110.00, actual0.getOfferPrice(), 1e-9);


        TwoWayPrice actual1 = this.testClazz.applyMarketUpdate(mockUpdateM1);
        assertEquals(Instrument.INSTRUMENT5, actual1.getInstrument());
        assertEquals(State.FIRM, actual1.getState());
        assertEquals(100.0 + 101.0, actual1.getBidAmount(), 1e-9);
        assertEquals(((100 *90) + (101.0 * 91.0))/(100.0 + 101.0), actual1.getBidPrice(), 1e-9);
        assertEquals(50.0 + 51.0, actual1.getOfferAmount(), 1e-9);
        assertEquals(   ((110 *50) + (111.0 * 51.0))/(50.0 + 51.0) , actual1.getOfferPrice(), 1e-9);


        TwoWayPrice actual2 = this.testClazz.applyMarketUpdate(mockUpdateM2);
        assertEquals(Instrument.INSTRUMENT5, actual2.getInstrument());
        assertEquals(State.FIRM, actual2.getState());
        assertEquals(100.0 + 101.0 + 102.0, actual1.getBidAmount(), 1e-9);
        assertEquals(((100.0 *90.0) + (101.0 * 91.0)+ (102.0 * 92.0))/(100.0 + 101.0 + 102.0), actual1.getBidPrice(), 1e-9);
        assertEquals(50.0 + 51.0 + 52.0, actual1.getOfferAmount(), 1e-9);
        assertEquals(   ((110 *50) + (111.0 * 51.0) + (112.0 *52.0))/(50.0 + 51.0 + 52.0) , actual1.getOfferPrice(), 1e-9);


    }

}